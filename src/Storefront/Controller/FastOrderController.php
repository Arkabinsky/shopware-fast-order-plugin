<?php

declare(strict_types=1);

namespace Esacore\FastOrder\Storefront\Controller;

use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;

/**
 * @RouteScope(scopes={"storefront"})
 */
class FastOrderController extends StorefrontController
{
    /**
     * @var LineItemFactoryRegistry
     */
    private $lineItemFactory;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var EntityRepositoryInterface
     */
    private $productRepository;

    /**
     * @var EntityRepositoryInterface
     */
    private $fastOrderRepository;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var string
     */
    private $productNumber;

    public function __construct(
        CartService $cartService,
        LineItemFactoryRegistry $lineItemFactory,
        EntityRepositoryInterface $productRepository,
        EntityRepositoryInterface $fastOrderRepository
    ) {
        $this->cartService = $cartService;
        $this->lineItemFactory = $lineItemFactory;
        $this->productRepository = $productRepository;
        $this->fastOrderRepository = $fastOrderRepository;
    }

    /**
     * @Route("/fast-order/create", name="frontend.fastOrder.create", options={"seo"="false"}, methods={"GET", "POST"})
     */
    public function create(SalesChannelContext $context, Request $request): Response
    {
        $errors = [];
        if ($request->request->has('productNumber') && $request->request->has('quantity')) {
            $this->quantity = (int)$request->request->get('quantity');
            $this->productNumber = $request->request->get('productNumber');
            $product = $this->findAvailableProduct($this->productNumber);
            $errors = $this->validate($product);
            if (!$errors) {
                $sessionId = $request->getSession()->getId();
                $this->fastOrderRepository->create(
                    [
                        [
                            'quantity' => $this->quantity,
                            'product' => ['id' => $product->getId()],
                            'sessionId' => $sessionId
                        ],
                    ],
                    Context::createDefaultContext()
                );

                $this->addToCard($product, $context);

                return $this->redirectToRoute('frontend.checkout.cart.page');
            }
        }

        return $this->renderStorefront('@Storefront/storefront/page/fast-order/index.html.twig', [
            'errors' => $errors,
            'quantity' => $this->quantity,
            'productNumber' => $this->productNumber,
        ]);
    }

    private function findAvailableProduct(string $productNumber): ?ProductEntity
    {
        /** @var EntityCollection $productCollection */
        $productCollection = $this->productRepository->search(
            (new Criteria())->addFilter(new EqualsFilter('productNumber', $productNumber))
                ->addFilter(new EqualsFilter('available', true))
                ->addFilter(new RangeFilter('availableStock', [
                    RangeFilter::GTE => $this->quantity
                ])),
            Context::createDefaultContext()
        );

        return $productCollection->first();
    }

    private function validate(?ProductEntity $product): array
    {
        $errors = [];
        if (!$product) {
            $errors[] = "There are no available products with number \"{$this->productNumber}\"";
        }

        if ($this->quantity < 10) {
            $errors[] = "You can't buy less than 10 products.";
        }

        return $errors;
    }

    private function addToCard(ProductEntity $product, SalesChannelContext $context): void
    {
        $lineItem = $this->lineItemFactory->create([
            'type' => 'product',
            'referencedId' => $product->getId(),
            'quantity' => $this->quantity,
        ], $context);

        $cart = $this->cartService->getCart($context->getToken(), $context);
        $this->cartService->add($cart, $lineItem, $context);
    }
}
