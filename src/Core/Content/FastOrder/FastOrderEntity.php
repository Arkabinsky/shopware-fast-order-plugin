<?php declare(strict_types=1);

namespace Esacore\FastOrder\Core\Content\FastOrder;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\Content\Product\ProductEntity;

class FastOrderEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * @var int
     */
    protected $quantity;
    
    /**
     * @var ProductEntity
     */
    protected $product;

    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    public function setSessionId(string $value): void
    {
        $this->sessionId = $value;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(string $value): void
    {
        $this->quantity = $value;
    }

    public function getProduct(): ProductEntity
    {
        return $this->product;
    }

    public function setProduct(ProductEntity $value): void
    {
        $this->product = $value;
    }
}
