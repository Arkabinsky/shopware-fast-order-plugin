<?php declare(strict_types=1);

namespace Esacore\FastOrder\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1613597721FastOrder extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1613597721;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `esacore_fast_order` (
              `id` BINARY(16) NOT NULL,
              `product_id` BINARY(16) NOT NULL,
              `quantity` INT(11) NOT NULL,
              `session_id` VARCHAR(255) NOT NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL DEFAULT NULL,
              PRIMARY KEY (`id`),
              CONSTRAINT `fk.fast_order.product.product_id` FOREIGN KEY (`product_id`)
                REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
