import './page/esacore-fast-order-list';

Shopware.Module.register('esacore-fast-order', {
    type: 'plugin',
    name: 'Custom',
    title: 'Fast order',
    description: 'Overview of "fast order" operations',
    color: '#62ff80',
    icon: 'default-object-lab-flask',

    routes: {
        list: {
            component: 'esacore-fast-order-list',
            path: 'list'
        },
    },

    navigation: [{
        label: 'Fast orders',
        color: '#62ff80',
        path: 'esacore.fast.order.list',
        icon: 'default-object-lab-flask'
    }],
});