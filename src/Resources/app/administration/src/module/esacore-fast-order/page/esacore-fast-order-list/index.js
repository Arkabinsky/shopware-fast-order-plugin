import template from './esacore-fast-order-list.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('esacore-fast-order-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            repository: null,
            fastOrders: null
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [{
                property: 'product.productNumber',
                dataIndex: 'product.productNumber',
                label: 'Product Number',
                allowResize: true
            }, {
                property: 'quantity',
                dataIndex: 'quantity',
                label: 'Quantity',
                inlineEdit: 'number',
                allowResize: true
            }, {
                property: 'sessionId',
                dataIndex: 'sessionId',
                label: 'Session ID',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }, {
                property: 'createdAt',
                dataIndex: 'createdAt',
                label: 'Created at',
                allowResize: true
            }];
        }
    },

    created() {
        this.repository = this.repositoryFactory.create('esacore_fast_order');

        this.repository
            .search(new Criteria(), Shopware.Context.api)
            .then((result) => {
                this.fastOrders = result;
            });
    }
});
